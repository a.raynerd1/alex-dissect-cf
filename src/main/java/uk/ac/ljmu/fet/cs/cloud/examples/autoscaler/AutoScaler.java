package uk.ac.ljmu.fet.cs.cloud.examples.autoscaler;

import java.util.ArrayList;
import java.util.Iterator;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.IaaSService;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VirtualMachine.State;

public class AutoScaler extends VirtualInfrastructure {
	public static int userPrivilege = 0;

	public AutoScaler(IaaSService cloud) {
		super(cloud);
	}

	/**
	 * Based on a user's set privileges, this autoscaler assigns them a set maximum
	 * number of VMs to use, and a number of cores that can assigned per VM. It also
	 * deletes any unused VMs to keep the limited numbers in use as efficient as
	 * possible.
	 * 
	 * The privilege value is set in the runconfigs as the final argument: For
	 * example: 2013-fall-week2-abbreviated.log 16 500
	 * uk.ac.ljmu.fet.cs.cloud.examples.autoscaler.AutoScaler 3
	 */
	@Override
	public void tick(long fires) {
		final Iterator<String> kinds = vmSetPerKind.keySet().iterator();
		while (kinds.hasNext()) {
			final String kind = kinds.next();
			final ArrayList<VirtualMachine> vmset = vmSetPerKind.get(kind);
			if (userPrivilege == 1) {
				if (vmset.size() > 0) {
					if (vmset.size() < 4) {
						requestVM(kind);
					}
					/**
					 * the value of max vms allowed changes depending on privileges, if the user's
					 * privilege is set to basic then they are capped at 4vms
					 */
				} else if (vmset.isEmpty()) {
					requestVM(kind);
				} else {
					destroyUnusedVMs(vmset);
					/**
					 * if no space is available for further vms, then any unused ones can be
					 * destroyed
					 */
				}
			} else if (userPrivilege == 2) {
				if (vmset.size() > 0) {
					if (vmset.size() < 8) {
						requestVM(kind);
					}
				} else if (vmset.isEmpty()) {
					requestVM(kind);
				} else {
					destroyUnusedVMs(vmset);
				}
			} else if (userPrivilege == 3) {
				if (vmset.size() > 0) {
					if (vmset.size() < 16) {
						requestVM(kind);
					}
				} else if (vmset.isEmpty()) {
					requestVM(kind);
				} else
					destroyUnusedVMs(vmset);
			} else {
				return;
			}
		}
	}

	private void destroyUnusedVMs(ArrayList<VirtualMachine> allVMs) {
		final ArrayList<VirtualMachine> unusedVMs = new ArrayList<VirtualMachine>();
		for (final VirtualMachine vm : allVMs) {
			if (vm.underProcessing.isEmpty() && vm.toBeAdded.isEmpty()) {
				unusedVMs.add(vm);
			}
		}
		for (int i = 0; i < unusedVMs.size(); i++) {
			destroyVM(unusedVMs.get(0));
		}
	}
}